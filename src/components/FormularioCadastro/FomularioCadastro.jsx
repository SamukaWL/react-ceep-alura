import { Component } from "react";

export default class FormularioCadastro extends Component {

  constructor(props) {
    super(props);
    this.titulo = "";
    this.texto = "";
  }

  _handleMudancaTitulo(evento) {
    evento.stopPropagation();
    this.titulo = evento.target.value;
  }

  _handleMudancaTexto(evento) {
    evento.stopPropagation();
    this.texto = evento.target.value;
  }

  _criarNota(evento) {
    evento.preventDefault();
    evento.stopPropagation();
    this.props.criarNota(this.titulo, this.texto);
  }

  render() {
    return (
      <form
      onSubmit={this._criarNota.bind(this)}
      >
        <input 
          className="input is-large" 
          type="text" 
          placeholder="titulo" 
          onChange={this._handleMudancaTitulo.bind(this)}
        />
        <textarea 
          className="textarea" 
          row="10" 
          placeholder="Escreva sua nota"
          onChange={this._handleMudancaTexto.bind(this)}
          >

        </textarea>
        <button className="button is-link is-large is-fullwidth">Criar nota</button>
      </form>

    );
  }
}