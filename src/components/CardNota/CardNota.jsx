import { Component } from 'react';
import "./estilo.css";
import "bulma/css/bulma.min.css";

export default class CardNota extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-content">
          <div className="content">
            <h5 className="title">{this.props.titulo}</h5>
            <p>{this.props.texto}</p>
          </div>
        </div>
      </div>
    );
  }
}

