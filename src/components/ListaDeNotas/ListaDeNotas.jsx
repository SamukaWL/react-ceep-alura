import { Component } from "react";
import CardNota from "../CardNota/CardNota";

export default class ListaDeNotas extends Component {
  
  render() {
    return (
      <div>
        {this.props.notas.map((notas, index) => {
          return (
            <div key={index}>
              <CardNota titulo={notas.titulo} texto={notas.texto}/>
            </div>
          );
        })}
      </div>
    );
  }
}