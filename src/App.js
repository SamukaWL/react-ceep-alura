import { Component } from "react";
import FormularioCadastro from "./components/FormularioCadastro/FomularioCadastro";
import ListaDeNotas from "./components/ListaDeNotas/ListaDeNotas";


export default class App extends Component {

  constructor(){
    super();
    this.state = {
      notas: []
    }
  }

  criarNota(titulo, texto) {
   const novaNota = {titulo, texto};
   const novoArrayNotas = [...this.state.notas, novaNota];
   const novoEstado = {
     notas:novoArrayNotas
   }
   this.setState(novoEstado)
  }

  render() {
    //JSX
    return (
      <section className="columns">
        {/* Component */}
        <div className="column"><FormularioCadastro criarNota={this.criarNota.bind(this)}/></div>
        <div className="column"><ListaDeNotas notas={this.state.notas}/></div>
      </section>
    );
  }
}

